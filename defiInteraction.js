const { ethers } = require('ethers');

const provider = new ethers.providers.JsonRpcProvider('https://rpc.ankr.com/eth');

const aavaEth = '0x7Fc66500c84A76Ad7e9c93437bFc5Ac33E2DDaE9';
const uniSwapEth = '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984'
const sushiSwapEth = '0x6b3595068778dd592e39a122f4f5a5cf09c90fe2'
const pancakeSwapEth = '0x152649eA73beAb28c5b49B26eb48f7EAD6d4c898'
const makerDaoEth = '0x9f8F72aA9304c8B593d555F12eF6589cC3A579A2'

const userAddress = '0xE37e799D5077682FA0a244D46E5649F71457BD09';
const daysBack = 170;

const transferEventSig = ethers.utils.id("Transfer(address,address,uint256)");

async function checkInteractionWithinDays(userAddress, daysBack) {
    const currentBlock = await provider.getBlockNumber();
    const blocksPerDay = 7164; // Rough estimate, adjust based on actual block time
    const startBlock = currentBlock - (blocksPerDay * daysBack);
    let fromBlock = startBlock;
    const toBlock = currentBlock;
    const maxBlocksPerQuery = 3000;

    let interactionsFound = false;

    while (fromBlock <= toBlock && !interactionsFound) {
        const queryToBlock = Math.min(fromBlock + maxBlocksPerQuery, toBlock);

        const transactions = await provider.getLogs({
            fromBlock: ethers.utils.hexlify(fromBlock),
            toBlock: ethers.utils.hexlify(queryToBlock),
            address: tokenContractAddress,
            topics: [transferEventSig, null, ethers.utils.hexZeroPad(userAddress, 32)],
        });
        
        if (transactions.length > 0) {
            console.log(`User has interacted in the last ${daysBack} days. Transaction details:`);
            transactions.forEach(tx => {
                console.log(`Block Number: ${tx.blockNumber}, Transaction Hash: ${tx.transactionHash}`);
            });
            interactionsFound = true;
        }

        // Increment the fromBlock for the next iteration to avoid infinite loop
        fromBlock = queryToBlock + 1;
    }

    if (!interactionsFound) {
        console.log(`User has not interacted in the last ${daysBack} days.`);
    }
}

checkInteractionWithinDays(userAddress, daysBack).catch(console.error);