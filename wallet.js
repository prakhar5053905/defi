const Moralis = require("moralis").default;
const {ethers} = require("ethers");
const { EvmChain } = require("@moralisweb3/common-evm-utils");

const getAge = async (walletAddress) => {
  try {
    await Moralis.start({
      apiKey:
        "IwGrsP6BGVVlWrbNvxGQIGYwG4rLfwPAlVBsBipKPDgIeJxX2A3W9Khr9lPKfpn9",
    });

    const response = await Moralis.EvmApi.wallets.getWalletActiveChains({
        "chains": [
            "0x1"
          ],
      "address": walletAddress,
    });
    // console.log(response.raw.active_chains[0]);
    if(response.raw.active_chains[0].first_transaction === null){
        console.log("Not found on this chain");
        return Error('Not found on this chain')
    }
    const isoStr = response.raw.active_chains[0].first_transaction.block_timestamp;
    const date = new Date(isoStr);

    const timestampToConvert = date.getTime() / 1000;
    const currentTimestamp = Math.floor(Date.now() / 1000);
    const differenceInSeconds = currentTimestamp - timestampToConvert;
    const days = Math.floor(differenceInSeconds / 86400);
    console.log(days, "days old");
  } catch (e) {
    console.error(e);
  }
};

const getTransactionCount = async(walletAddress) => {
    const provider = new ethers.providers.JsonRpcProvider("https://api.baobab.klaytn.net:8651");
    const user = await provider.getTransactionCount("0x74f9478fbAD90e25371C32428bf21c439b91C865");
    console.log(user, "Total Transactions on this chain");
}

const getAllTransactions = async() => {
    try {
        await Moralis.start({
          apiKey: "IwGrsP6BGVVlWrbNvxGQIGYwG4rLfwPAlVBsBipKPDgIeJxX2A3W9Khr9lPKfpn9"
        });
      
        const address = "0x26fcbd3afebbe28d0a8684f790c48368d21665b5";

        const chain = EvmChain.ETHEREUM;
      
        const response = await Moralis.EvmApi.transaction.getWalletTransactions({
          address,
          chain,
        });
      
        console.log(response.toJSON());
      }catch (e) {
        console.error(e);
    }
}

const getBalance = async(walletAddress) => {
    const provider = new ethers.providers.JsonRpcProvider("https://api.baobab.klaytn.net:8651");
    const balance = await provider.getBalance(walletAddress)
    console.log("Balance:",ethers.utils.formatEther(balance.toString()).toString(), "On this chain")
}

getBalance("0x74f9478fbAD90e25371C32428bf21c439b91C865")
// getAllTransactions()
// getAge("0x95222290DD7278Aa3Ddd389Cc1E1d165CC4BAfe5")
// getTransactionCount("0x74f9478fbAD90e25371C32428bf21c439b91C865")
